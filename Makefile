# Ocaml-specific definitions

OCAMLC=ocamlc
OCAMLOPT=ocamlopt

INCLUDES= 
OCAMLFLAGS= $(INCLUDES) 
OCAMLOPTFLAGS= $(INCLUDES) 


# Common rules
.SUFFIXES: .ml .mli .cmo .cmi .cmx

.ml.cmo:
	$(OCAMLC) $(OCAMLFLAGS) -c $<

.mli.cmi:
	$(OCAMLC) $(OCAMLFLAGS) -c $<

.ml.cmx:
	$(OCAMLOPT) $(OCAMLOPTFLAGS) -c $<


# project-specific definitions

PROGNAME=eulerpb2

BYTE_OBJS=$(PROGNAME).cmo
NATIVE_OBJS=$(PROGNAME).cmx


# targets

all: byte-code native-code

byte-code: $(BYTE_OBJS)
	$(OCAMLC) -o $(PROGNAME).byte $(OCAMLFLAGS) $(BYTE_OBJS)

native-code: $(NATIVE_OBJS)
	$(OCAMLOPT) -o $(PROGNAME).native $(OCAMLFLAGS) $(NATIVE_OBJS)

clean:
	-rm -f *.cm[iox] *.o

distclean:
	-rm -f *.byte *.native
	-rm -f *.cm[iox] *.o
