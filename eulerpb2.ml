(* 
This program gives a generalized solution to the problem:

"Find the sum of all Fibonacci numbers which are even and below n, where Fibonacci
numbers are defined as such:

F(1) = 1
F(2) = 2
F(n) = F(n-1) + F(n-2)"
*)


(* this function returns the Fibonacci number F(n) for n, with F(1) = 1, F(2) = 2 
   and F(n) = F(n-1) + F(n-2) for n>2 *)

let rec fib = function
  | 1 -> 1
  | 2 -> 2
  | n -> fib (n - 1) + fib (n - 2) ;;


(* this function computes the sum of the Fibonacci numbers which are even and less than n 
   it uses a recursive function which iterates thru all the Fibonacci numbers and adds
   only the even ones to the final result; iterations begin at 1 because of problem formulation *)

let sum_even_fib n = 

  let rec sum_even_fib_rec i = 
    let fibnum = fib i in 
      if (fibnum >= n) then 0
      else
        if (fibnum mod 2 = 0) then 
           fibnum + sum_even_fib_rec (i + 1) 
        else 
           sum_even_fib_rec (i + 1) in 
  
    sum_even_fib_rec 1;;


(* Main program. Takes one argument on the command line.
   Print usage instructions if invoked without arguments or with bad arguments  *)

let main () =

  try

    let n = int_of_string Sys.argv.(1) in 
      print_int (sum_even_fib n); print_newline ()

  with _ -> Printf.printf "usage : %s n\n" Sys.argv.(0) ;;

main ();;
